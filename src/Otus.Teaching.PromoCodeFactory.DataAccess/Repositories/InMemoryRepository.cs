﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class InMemoryRepository<T>
        : IRepository<T>
        where T: BaseEntity
    {
        protected IEnumerable<T> Data { get; set; }

        public InMemoryRepository(IEnumerable<T> data)
        {
            Data = data;
        }
        
        public Task<IEnumerable<T>> GetAllAsync()
        {
            return Task.FromResult(Data);
        }

        public Task<T> GetByIdAsync(Guid id)
        {
            return Task.FromResult(Data.FirstOrDefault(x => x.Id == id));
        }

        public Task<IEnumerable<T>> Create(T entity)
        {
            IEnumerable<T> res = Data.ToList().Concat(new[] { entity });
            return Task.FromResult (res);
        }

        public Task<IEnumerable<T>> Update(T entity)
        {
            T dateReq = Data.FirstOrDefault(x => x.Id == entity.Id);
            if (dateReq == null) throw new ArgumentException($"Сотрудника с ID = {entity.Id} не существует");
            IEnumerable<T> res = Data.Select(x => (x.Id == entity.Id) ? entity : x);
            return Task.FromResult(res);
        }

        public Task<IEnumerable<T>> Delete(Guid id)
        {
            T dateReq = Data.FirstOrDefault(x => x.Id == id);
            if (dateReq == null) throw new ArgumentException($"Сотрудника с ID = {id} не существует");
            IEnumerable<T> res = Data.Where(x => x.Id != id);
            return Task.FromResult(res);
        }
    }
}